<?php

namespace Hermes\Settings\Models;

use Illuminate\Database\Eloquent\Model;

class SettingOption extends Model
{
    protected $table = "setting_options";
    protected $guarded = ["id", ""];
    protected $fillable = [
        "setting_id",
        "label",
        "value"
    ];

    public function setting()
    {
        return $this->belongsTo("Hermes\Settings\Models\Setting");
    }
}
