<?php

namespace Hermes\Settings\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = "settings";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "setting_group_id",
        "setting_type_id",
        "name",
        "label",
        "description",
        "value"
    ];

    public function settingGroup()
    {
        return $this->belongsTo(SettingGroup::class);
    }

    public function settingType()
    {
        return $this->belongsTo(SettingType::class);
    }

    public function settingOptions()
    {
        return $this->hasMany(SettingOption::class);
    }
}