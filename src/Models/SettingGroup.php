<?php

namespace Hermes\Settings\Models;

use Illuminate\Database\Eloquent\Model;

class SettingGroup extends Model
{
    protected $table = "setting_groups";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "name",
        "label",
        "description"
    ];

    public function settings()
    {
        return $this->hasMany("Hermes\Settings\Models\Setting");
    }
}
