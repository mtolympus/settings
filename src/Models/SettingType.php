<?php

namespace Hermes\Settings\Models;

use Illuminate\Database\Eloquent\Model;

class SettingType extends Model
{
    protected $table = "setting_types";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "name",
        "label"
    ];

    public function settings()
    {
        return $this->hasMany("Hermes\Settings\Models\Setting");
    }
}
