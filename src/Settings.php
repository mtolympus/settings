<?php

namespace Hermes\Settings;

use Hermes\Settings\Models\Setting;
use Hermes\Settings\Models\SettingType;
use Hermes\Settings\Models\SettingGroup;
use Hermes\Settings\Models\SettingOption;

class Settings
{
    // Group fields
    private $requiredGroupFields;
    private $optionalGroupFields;

    // Setting fields
    private $requiredSettingFields;
    private $optionalSettingFields;

    /**
     * The settings service
     * 
     * This class contains all of the functionality this package contains and
     * is bootstrapped to the application in this package's ServiceProvider
     * 
     * @author      Nick Verheijen <verheijen.webdevelopment@gmail.com>
     * @version     1.0.0
     */
    public function __construct()
    {
        // Define required & optional fields for the SettingGroup model
        $this->requiredGroupFields = [
            "name", 
            "label"
        ];
        $this->optionalGroupFields = [
            "description"
        ];

        // Define required & optional fields for the Setting model
        $this->requiredSettingFields = [
            "type",
            "name", 
            "label",
        ];
        $this->optionalSettingFields = [
            "setting_type_id", 
            "setting_group_id", 
            "description", 
            "value"
        ];
    }
    
    // ------------------------------------------------------------
    // Getters
    // ------------------------------------------------------------

    /**
     * All settings
     * Returns a collection of all the settings currently in the database
     * 
     * @return          Illuminate\Support\Collection       A collection of all settings that exist
     */
    public function all()
    {
        return Setting::all();
    }

    /**
     * Get
     * Finds and returns a setting by it's name
     * 
     * @param           string                              The name of the setting
     * @param           string                              Prefix we might have added
     * @return          App\Models\Setting                  The setting or false if we failed to find it
     */
    public function get(string $settingName, string $prefix = null)
    {
        // Determine the name of the setting
        $name = is_null($prefix) ? $settingName : $prefix . "_". $settingName;
        
        // Find the setting
        $setting = Setting::where("name", $name)->first();

        // If we found it
        if ($setting)
        {
            // Return the value
            return $this->getSettingValue($setting);
        }

        // If we ended up here the setting could not be found
        // or it does not have a type. Return false either way.
        return false;
    }

    /**
     * Get setting
     * 
     * @param       string                                  Name of the setting
     * @param       string                                  Prefix to use
     * @return      Hermes\Settings\Models\Setting
     */
    public function getSetting(string $settingName, string $prefix = null)
    {
        // Determine the name of the setting
        $name = is_null($prefix) ? $settingName : $prefix . "_". $settingName;
        
        // Find the setting
        $setting = Setting::where("name", $name)->first();

        // If we found the setting
        if ($setting)
        {
            // Preload relationship data
            $setting->load("settingType");
            $setting->load("settingGroup");
            $setting->load("settingOptions");

            // Add the formatted value to the setting
            $setting->formatted_value = $this->getSettingValue($setting);

            // And return the setting
            return $setting;
        }

        // Return false by default
        return false;
    }

    public function getSettingValue(Setting $setting)
    {
        // Switch between supported types
        switch ($setting->settingType->name)
        {
            default:
            case "text":
            case "select":
                return $setting->value;
            break;
            case "boolean":
                return (bool) $setting->value;
            break;
            case "number":
                return (int) $setting->value;
            break;
            case "file":
            case "image":
                return asset($setting->value);
            break;
        }

        return false;
    }

    /**
     * Get groups
     * 
     * @return          Illuminate\Support\Collection
     */
    public function getGroups()
    {
        return SettingGroup::with("settings")->get();
    }

    /**
     * Get group by name
     * 
     * @param           String                              The name of the setting group
     * @return          App\Models\SettingGroup             The setting group or false if we failed to find it
     */
    public function getGroup($settingGroupName)
    {
        // Find the group we're looking for
        $group = SettingGroup::where("name", $settingGroupName)->first();
        
        // If we found the group
        if ($group)
        {
            // Load all the relationships
            $group->load("settings");
            $group->load("settings.settingType");
            $group->load("settings.settingOptions");
            
            // Return the group
            return $group;
        }

        // If we could not find the group return false
        return false;
    }

    /**
     * Get all setting types
     * 
     * @return      Illuminate\Support\Collection       
     */
    public function getTypes()
    {
        return SettingType::all();
    }

    /**
     * Get setting type by it's name
     * 
     * @param       string                              The name of the setting type
     * @return      Hermes\Settings\SettingType
     */
    public function getType($typeName)
    {
        return SettingType::where("name", $typeName)->first();
    }

    // ------------------------------------------------------------
    // Setters
    // ------------------------------------------------------------

    /**
     * Set setting's value (using the Setting object)
     * 
     * @param           App\Models\Setting                  The setting we want to set the value of
     * @param           mixed                               The new value of the setting
     * @return          App\Models\Setting                  The updated setting
     */
    public function set(Setting $setting, $value)
    {
        // Update the setting and persist the changes
        $setting->value = $value;
        $setting->save();

        // Return the setting
        return $setting;
    }

    /**
     * Set setting's value (using it's name)
     * 
     * @param           string                              The name of the setting
     * @param           mixed                               The desired value of the setting
     * @param           string|null                         The prefix we should use for the setting
     * @return          App\Models\Setting|false            The updated setting
     */
    public function setByName(string $settingName, $value, string $prefix = null)
    {
        // Determine the name of the setting
        $name = is_null($prefix) ? $settingName : $prefix . "_" . $settingName;

        // Attempt to find the setting using it's name
        $setting = Setting::where("name", $name)->first();
        if ($setting)
        {
            // Re-use the set method
            return $this->set($setting, $value);
        }

        // Return false by default
        return false;
    }

    // ------------------------------------------------------------
    // Creating stuff
    // ------------------------------------------------------------

    /**
     * Create setting groups
     * Meant to be used in the registerPackage method of the Admin Module or in whatever 
     * other situation you'd want to bulk create setting groups.
     * 
     * @param           array                                   Data representing settings groups and possibly settings 
     * @return          Illuminate\Support\Collection           Collection of the created setting groups
     */
    public function createSettingGroups(array $data)
    {
        $out = [];

        // If there is data to process
        if (count($data) > 0)
        {
            // Loop through each data entry
            foreach ($data as $entry)
            {
                // If the entry data is valid
                if ($this->settingGroupEntryIsValid($entry))
                {
                    // Create the settings group
                    $group = SettingGroup::create($this->composeSettingGroupData($entry));

                    // If the group contains settings
                    if (array_key_exists("settings", $entry) and count($entry["settings"]) > 0)
                    {
                        // Create those as well
                        $this->createSettingsForGroup($group, $entry["settings"]);
                    }

                    // Add it to the output
                    $out[] = $group;
                }
            }
        }

        // Return the output as a collection
        return collect($out);
    }

    /**
     * Create settings
     * If you want to bulk create settings that don't belong to a group
     * 
     * @param           array                                   Data representing settings
     * @return          Illuminate\Support\Collection           Collection of the created settings
     */
    public function createSettings(array $data)
    {
        $out = [];
        
        // If there is data to process
        if (count($data) > 0)
        {
            // Loop through each data entry
            foreach ($data as $entry)
            {
                // If the entry data is valid
                if ($this->settingEntryIsValid($entry))
                {
                    // Make sure the setting does not exist yet
                    $existingSetting = Setting::where("name", $entry["name"])->first();
                    if (!$existingSetting)
                    {
                        // Create the setting
                        $setting = Setting::create($this->composeSettingData($entry));

                        // Add it to the output
                        $out[] = $setting;
                    }
                }
            }
        }

        // Return the output as a collection
        return collect($out);
    }

    /**
     * Create settings for group
     * If you want to bulk create settings for an existing SettingGroup
     * 
     * @param           SettingGroup                            The group we want to add the links to
     * @param           array                                   Data representing the settings
     * @return          Illuminate\Support\Collection           Collection of the created settings
     */
    public function createSettingsForGroup(SettingGroup $group, array $data)
    {
        $out = [];
        
        // We want the user to provider the least amount of data to get the most work done
        // So we will update the data we received and add the group's id to it
        $updatedData = [];
        if (count($data) > 0)
        {
            foreach ($data as $entry)
            {
                $entry["setting_group_id"] = $group->id;
                $updatedData[] = $entry;
            }
        }

        // Re-use the create settings method
        $this->createSettings($updatedData);

        // Return the output as a collection
        return collect($out);
    }
    
    // ------------------------------------------------------------
    // Private methods
    // ------------------------------------------------------------

    private function settingGroupEntryIsValid(array $data)
    {
        foreach ($this->requiredGroupFields as $requiredField)
        {
            if (!array_key_exists($requiredField, $data))
            {
                return false;
            }
        }
        
        return true;
    }

    private function composeSettingGroupData(array $data)
    {
        $out = [];

        foreach ($this->requiredGroupFields as $requiredField)
        {
            $out[$requiredField] = $data[$requiredField];
        }

        foreach ($this->optionalGroupFields as $optionalField)
        {
            if (array_key_exists($optionalField, $data))
            {
                $out[] = $data[$optionalField];
            }
        }

        return $out;
    }

    private function settingEntryIsValid(array $data)
    {
        foreach ($this->requiredSettingFields as $requiredField)
        {
            if (!array_key_exists($requiredField, $data))
            {
                return false;
            }
        }

        return true;
    }

    private function composeSettingData(array $data)
    {
        $out = [];

        // If there is data to process
        if (count($data) > 0)
        {
            // Process all of the required fields
            foreach ($this->requiredSettingFields as $requiredField)
            {
                // By simply adding the key => value pair to the output
                $out[$requiredField] = $data[$requiredField];
            }

            // Process all of the optional fields
            foreach ($this->optionalSettingFields as $optionalField)
            {
                // If the optional field is present
                if (array_key_exists($optionalField, $data))
                {
                    // If it's the type field
                    if ($optionalField == "type")
                    {
                        // Attempt to find the type by it's name
                        $type = SettingType::where("name", $data["type"])->first();

                        // If the type does not exist, create it
                        if (!$type)
                        {
                            // Create it
                            $type = SettingType::create([
                                "name" => $data["type"],
                                "label" => ucfirst(strtolower(str_replace("_", " ", $data["type"])))
                            ]);
                        }

                        // Add the SettingType's id
                        $out["setting_type_id"] = $type->id;
                    }
                    // If it's any other field
                    else
                    {
                        // Simply copy pasta
                        $out[$optionalField] = $data[$optionalField];
                    }
                } 
            }
        }

        // Return the output data
        return $out;
    }
}