<?php

namespace Hermes\Settings\Providers;

use Hermes\Settings\Settings;
use Illuminate\Support\ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Setup migration loading
        // $this->loadMigrationsFrom(__DIR__."/../Database/migrations");

        // Setup migration publishing in case modifications to the database structure
        // need to be made.
        $this->publishes([__DIR__."/../Database/migrations" => database_path("migrations")], "migrations");

        // Setup seeder publishing
        $this->publishes([__DIR__."/../Database/seeds" => database_path("seeds")], "seeds");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register the Settings service as a singleton to the IoC
        $this->app->singleton("settings", function() {
            return new Settings;
        });
    }
}