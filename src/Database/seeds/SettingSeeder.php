<?php

use Hermes\Settings\Models\Setting;
use Hermes\Settings\Models\SettingType;
use Hermes\Settings\Models\SettingGroup;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->emptyDatabase();
        $this->seedDatabase();
    }

    private function emptyDatabase()
    {
        DB::table("settings")->delete();
        DB::table("setting_types")->delete();
        DB::table("setting_groups")->delete();
    }

    private function seedDatabase()
    {
        // Create the setting types
        $t_text = SettingType::create(["name" => "text", "label" => "Text"]);
        $t_number = SettingType::create(["name" => "number", "label" => "Number"]);
        $t_boolean = SettingType::create(["name" => "boolean", "label" => "Boolean"]);
        $t_select = SettingType::create(["name" => "select", "label" => "Select"]);
        $t_image = SettingType::create(["name" => "image", "label" => "Image"]);
        $t_file = SettingType::create(["name" => "file", "label" => "File"]);
    }
}