# Laravel Settings

This package allows you to store settings in the database. It also provides some simple (and optional) grouping.
Besides having access to these settings programatically the package also provides a UI with accompanying backend to manage the application's settings.

# Installation


Require the package in your application
```
composer require hermes\settings
```

Publish the views & assets by running the following command

```
php artisan 
```

You're done!

# Usage

We personally like to create a seeder for the initial settings which need to be managed with the GUI of the backend of our applications

More to be added...